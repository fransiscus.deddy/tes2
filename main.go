package main

import (
	"strconv"
)

func main() {

	total := Multiplication("39")

	println("total => ", total)
}

// Multiplication ...
// (int32, bool)
func Multiplication(numb string) int {

	angka1, _ := strconv.Atoi(numb[0:1])
	angka2, _ := strconv.Atoi(numb[1:2])
	total := angka1 * angka2

	totalInString := strconv.Itoa(total)
	// println(angka1, " ", angka2)
	// println("", total)

	if len(totalInString) > 1 {
		total = Multiplication(totalInString)
	}
	return total
	// return 0, true
}
